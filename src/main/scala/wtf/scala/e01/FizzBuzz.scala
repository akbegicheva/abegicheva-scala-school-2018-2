package wtf.scala.e01

object FizzBuzz {

  /**
    * Return "fizz", "buzz", "fizzbuzz" or number between 0 (inclusive) and n (exclusive).
    * For a given natural number greater than zero return:
    *   "fizzbuzz" if the number is dividable by 3 and 5
    *   "buzz" if the number is dividable by 5
    *   "fizz" if the number is dividable by 3
    * the same number if no other requirement is fulfilled
    * @param n
    * @throws IllegalArgumentException if n < 1
    * @return seq of fizzbuzz strings
    */
  @throws[IllegalArgumentException]
  private[e01] def fizzBuzzUntil(n: Int): Seq[String] = {
    require(n >= 1, "n must be more or equal to 1")
    (0 until n).map(result => result match{
      case result if result % 15 == 0 => "fizzbuzz"
      case result if result % 5 == 0 => "buzz"
      case result if result % 3 == 0 => "fizz"
      case _ => result.toString
    })
  }

  def main(args: Array[String]): Unit = {
    println(fizzBuzzUntil(100).mkString(" "))
  }
}
